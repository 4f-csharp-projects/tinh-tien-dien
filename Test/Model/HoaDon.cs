﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Test.Model
{
    public class HoaDon
    {
        private string _khachHang;
        private int _soNhanKhau, _dinhMuc, _vuotDinhMuc, _giaTienDinhMuc, _chiSoCu, _chiSoMoi, _tieuThu, _tienSuDung, _phiBVMT, _tienPhaiTra;

        public string KhachHang { get; set; }
        public int SoNhanKhau { get => _soNhanKhau; set => _soNhanKhau = value; }
        public int DinhMuc { get => _dinhMuc; set => _dinhMuc = value; }
        public int VuotDinhMuc { get => _vuotDinhMuc; set => _vuotDinhMuc = value; }
        public int GiaTienDinhMuc { get => _giaTienDinhMuc; set => _giaTienDinhMuc = value; }
        public int ChiSoCu { get => _chiSoCu; set => _chiSoCu = value; }
        public int ChiSoMoi { get => _chiSoMoi; set => _chiSoMoi = value; }
        public int TieuThu { get => _tieuThu; set => _tieuThu = value; }
        public int TienSuDung { get => _tienSuDung; set => _tienSuDung = value; }
        public int PhiBVMT { get => _phiBVMT; set => _phiBVMT = value; }
        public int TienPhaiTra { get => _tienPhaiTra; set => _tienPhaiTra = value; }

        public HoaDon() {
            _khachHang = "";
            _soNhanKhau = 0;
            _dinhMuc = 0;
            _vuotDinhMuc = 0;
            _giaTienDinhMuc = 0;
            _chiSoCu = 0;
            _chiSoMoi = 0;
            _tieuThu = 0;
            _tienSuDung = 0;
            _phiBVMT = 0;
            _tienPhaiTra = 0;
        }

        public HoaDon(string khachHang, int soNhanKhau, int dinhMuc, int vuotDinhMuc, 
            int giaTienDinhMuc, int chiSoCu, int chiSoMoi, int tieuThu, 
            int tienSuDung, int phiBVMT, int tienPhaiTra)
        {
            _khachHang = khachHang;
            _soNhanKhau = soNhanKhau;
            _dinhMuc = dinhMuc;
            _vuotDinhMuc = vuotDinhMuc;
            _giaTienDinhMuc = giaTienDinhMuc;
            _chiSoCu = chiSoCu;
            _chiSoMoi = chiSoMoi;
            _tieuThu = tieuThu;
            _tienSuDung = tienSuDung;
            _phiBVMT = phiBVMT;
            _tienPhaiTra = tienPhaiTra;
        }
    }
}
