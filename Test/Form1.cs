﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Test.Model;

namespace Test
{
    public partial class Form1 : Form
    {
        // Tạo 1 instance của hoá đơn
        HoaDon hd = new HoaDon();

        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            // Thêm các item cho combo box
            khachHangComboBox.Items.Add("Tâm Gà");
            khachHangComboBox.Items.Add("Yasuo");
            khachHangComboBox.Items.Add("Riven");
            khachHangComboBox.Items.Add("Heimerdinger");
        }

        private void TinhHoaDon()
        {
            // Xoá các error provider nếu ấn lại nút "Tính" một lần nữa
            err.Clear();

            if (khachHangComboBox.Text == "")
            {
                err.SetError(khachHangComboBox, "Chưa có tên khách hàng!");
                khachHangComboBox.Focus();
            }
            else
            {
                hd.KhachHang = khachHangComboBox.Text;
                hd.SoNhanKhau = int.Parse(soNhanKhauTxtBox.Text);
            }

            // Check số nhân khẩu
            if (hd.SoNhanKhau < 4)
            {
                hd.DinhMuc = 12;
                hd.GiaTienDinhMuc = 4000;
                metKhoiTrongDinhMucTxtBox.Text = "12";
                noteTxtBox.Text = "Nếu số nhân khẩu < 4 thì định mức mặc định là 12.";
            }
            else
            {
                hd.DinhMuc = 15;
                hd.GiaTienDinhMuc = 8000;
                metKhoiTrongDinhMucTxtBox.Text = "15";
                noteTxtBox.Text = "Nếu số nhân khẩu >= 4 thì định mức mặc định là 15.";
            }

            // Kiển tra tính hợp lý của chỉ số cũ và mới
            hd.ChiSoCu = int.Parse(chiSoCuTxtBox.Text);
            hd.ChiSoMoi = int.Parse(chiSoMoiTxtBox.Text);

            if (hd.ChiSoCu < hd.ChiSoMoi)
            {
                // Tính số mét khối tiêu thụ
                hd.TieuThu = hd.ChiSoMoi - hd.ChiSoCu;
                tieuThuMetKhoiTxtBox.Text = hd.TieuThu.ToString();
            }
            else
            {
                err.SetError(chiSoCuTxtBox, "Chỉ số cũ không thể lớn hơn hoặc bằng chỉ số mới! Yêu cầu nhập lại");
                hd.ChiSoCu = hd.ChiSoMoi = 0;
                chiSoCuTxtBox.Clear();
                chiSoMoiTxtBox.Clear();
                chiSoCuTxtBox.Focus();
            }

            // Tính số mét khối vượt định mức
            hd.VuotDinhMuc = hd.TieuThu - hd.DinhMuc;
            if (hd.VuotDinhMuc > 0) metKhoiVuotiDinhMucTxtBox.Text = hd.VuotDinhMuc.ToString();
            else
            {
                hd.VuotDinhMuc = 0;
                metKhoiVuotiDinhMucTxtBox.Text = hd.VuotDinhMuc.ToString();
            }

            // Tính tổng tiền sử dụng
            hd.TienSuDung = hd.TieuThu * hd.GiaTienDinhMuc;
            tongTienSuDungTxtBox.Text = hd.TienSuDung.ToString();

            // Tính phí bảo vệ môi trường
            hd.PhiBVMT = hd.TienSuDung / 10;
            phiBaoVeMoiTruongTxtBox.Text = hd.PhiBVMT.ToString();

            // Tính tiền phải trả:
            hd.TienPhaiTra = hd.TienSuDung + hd.PhiBVMT;
            tongTienTxtBox.Text = hd.TienPhaiTra.ToString();
        }

        private void calBtn_Click(object sender, EventArgs e)
        {
            TinhHoaDon();
        }
        
        private void printBtn_Click(object sender, EventArgs e)
        {
            printTxtBox.Text =
                "Khách hàng: " + hd.KhachHang + "\r\n" +
                "Số nhân khẩu: " + hd.SoNhanKhau + "\r\n" +
                "Mét khối tiêu thụ: " + hd.TieuThu + "\r\n" +
                "Tiền phải trả: " + hd.TienPhaiTra + " VNĐ" + "\r\n";
        }
        
        private void exitBtn_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }
    }
}
