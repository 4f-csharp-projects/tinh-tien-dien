﻿namespace Test
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.calBtn = new System.Windows.Forms.Button();
            this.printBtn = new System.Windows.Forms.Button();
            this.exitBtn = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.printTxtBox = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.chiSoCuTxtBox = new System.Windows.Forms.TextBox();
            this.chiSoMoiTxtBox = new System.Windows.Forms.TextBox();
            this.tieuThuMetKhoiTxtBox = new System.Windows.Forms.TextBox();
            this.metKhoiTrongDinhMucTxtBox = new System.Windows.Forms.TextBox();
            this.soNhanKhauTxtBox = new System.Windows.Forms.TextBox();
            this.metKhoiVuotiDinhMucTxtBox = new System.Windows.Forms.TextBox();
            this.tongTienTxtBox = new System.Windows.Forms.TextBox();
            this.phiBaoVeMoiTruongTxtBox = new System.Windows.Forms.TextBox();
            this.tongTienSuDungTxtBox = new System.Windows.Forms.TextBox();
            this.khachHangComboBox = new System.Windows.Forms.ComboBox();
            this.noteTxtBox = new System.Windows.Forms.TextBox();
            this.err = new System.Windows.Forms.ErrorProvider(this.components);
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.err)).BeginInit();
            this.SuspendLayout();
            // 
            // calBtn
            // 
            this.calBtn.Location = new System.Drawing.Point(3, 3);
            this.calBtn.Name = "calBtn";
            this.calBtn.Size = new System.Drawing.Size(75, 23);
            this.calBtn.TabIndex = 0;
            this.calBtn.Text = "Tính";
            this.calBtn.UseVisualStyleBackColor = true;
            this.calBtn.Click += new System.EventHandler(this.calBtn_Click);
            // 
            // printBtn
            // 
            this.printBtn.Location = new System.Drawing.Point(84, 3);
            this.printBtn.Name = "printBtn";
            this.printBtn.Size = new System.Drawing.Size(75, 23);
            this.printBtn.TabIndex = 1;
            this.printBtn.Text = "In";
            this.printBtn.UseVisualStyleBackColor = true;
            this.printBtn.Click += new System.EventHandler(this.printBtn_Click);
            // 
            // exitBtn
            // 
            this.exitBtn.Location = new System.Drawing.Point(165, 3);
            this.exitBtn.Name = "exitBtn";
            this.exitBtn.Size = new System.Drawing.Size(75, 23);
            this.exitBtn.TabIndex = 2;
            this.exitBtn.Text = "Thoát";
            this.exitBtn.UseVisualStyleBackColor = true;
            this.exitBtn.Click += new System.EventHandler(this.exitBtn_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.calBtn);
            this.flowLayoutPanel1.Controls.Add(this.printBtn);
            this.flowLayoutPanel1.Controls.Add(this.exitBtn);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(34, 425);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(256, 33);
            this.flowLayoutPanel1.TabIndex = 3;
            // 
            // printTxtBox
            // 
            this.printTxtBox.AcceptsReturn = true;
            this.printTxtBox.BackColor = System.Drawing.Color.Khaki;
            this.printTxtBox.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.printTxtBox.Location = new System.Drawing.Point(449, 365);
            this.printTxtBox.Multiline = true;
            this.printTxtBox.Name = "printTxtBox";
            this.printTxtBox.ReadOnly = true;
            this.printTxtBox.Size = new System.Drawing.Size(382, 176);
            this.printTxtBox.TabIndex = 23;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Segoe UI Semilight", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(258, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(326, 37);
            this.label1.TabIndex = 1;
            this.label1.Text = "Tính tiền nước hàng tháng";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(28, 135);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(74, 13);
            this.label2.TabIndex = 4;
            this.label2.Text = "Số nhân khẩu";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(605, 177);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(89, 13);
            this.label3.TabIndex = 10;
            this.label3.Text = "Tiêu thụ mét khối";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(388, 177);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(55, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Chỉ số mới";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(31, 177);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(51, 13);
            this.label5.TabIndex = 6;
            this.label5.Text = "Chỉ số cũ";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(28, 212);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(137, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Số mét khối trong định mức";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(28, 252);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(134, 13);
            this.label7.TabIndex = 14;
            this.label7.Text = "Số mét khối vượt định mức";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(31, 326);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(138, 13);
            this.label8.TabIndex = 18;
            this.label8.Text = "Phí bảo vệ môi trường 10%:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(28, 289);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(96, 13);
            this.label9.TabIndex = 16;
            this.label9.Text = "Tổng tiền sử dụng:";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(28, 98);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(65, 13);
            this.label10.TabIndex = 2;
            this.label10.Text = "Khách hàng";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Segoe UI Semilight", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(549, 123);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(140, 13);
            this.label11.TabIndex = 15;
            this.label11.Text = "Vượt định mức: 8000đ/người";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Segoe UI Semilight", 8.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(549, 103);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(145, 13);
            this.label12.TabIndex = 16;
            this.label12.Text = "Trong định mức: 4000đ/người";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Location = new System.Drawing.Point(31, 365);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(93, 13);
            this.label13.TabIndex = 20;
            this.label13.Text = "Tổng tiền phải trả:";
            // 
            // chiSoCuTxtBox
            // 
            this.chiSoCuTxtBox.Location = new System.Drawing.Point(192, 170);
            this.chiSoCuTxtBox.Name = "chiSoCuTxtBox";
            this.chiSoCuTxtBox.Size = new System.Drawing.Size(190, 20);
            this.chiSoCuTxtBox.TabIndex = 7;
            // 
            // chiSoMoiTxtBox
            // 
            this.chiSoMoiTxtBox.Location = new System.Drawing.Point(449, 174);
            this.chiSoMoiTxtBox.Name = "chiSoMoiTxtBox";
            this.chiSoMoiTxtBox.Size = new System.Drawing.Size(150, 20);
            this.chiSoMoiTxtBox.TabIndex = 9;
            // 
            // tieuThuMetKhoiTxtBox
            // 
            this.tieuThuMetKhoiTxtBox.Location = new System.Drawing.Point(700, 174);
            this.tieuThuMetKhoiTxtBox.Name = "tieuThuMetKhoiTxtBox";
            this.tieuThuMetKhoiTxtBox.ReadOnly = true;
            this.tieuThuMetKhoiTxtBox.Size = new System.Drawing.Size(131, 20);
            this.tieuThuMetKhoiTxtBox.TabIndex = 11;
            // 
            // metKhoiTrongDinhMucTxtBox
            // 
            this.metKhoiTrongDinhMucTxtBox.ForeColor = System.Drawing.Color.Red;
            this.metKhoiTrongDinhMucTxtBox.Location = new System.Drawing.Point(192, 212);
            this.metKhoiTrongDinhMucTxtBox.Name = "metKhoiTrongDinhMucTxtBox";
            this.metKhoiTrongDinhMucTxtBox.ReadOnly = true;
            this.metKhoiTrongDinhMucTxtBox.Size = new System.Drawing.Size(190, 20);
            this.metKhoiTrongDinhMucTxtBox.TabIndex = 13;
            // 
            // soNhanKhauTxtBox
            // 
            this.soNhanKhauTxtBox.Location = new System.Drawing.Point(192, 135);
            this.soNhanKhauTxtBox.Name = "soNhanKhauTxtBox";
            this.soNhanKhauTxtBox.Size = new System.Drawing.Size(192, 20);
            this.soNhanKhauTxtBox.TabIndex = 5;
            // 
            // metKhoiVuotiDinhMucTxtBox
            // 
            this.metKhoiVuotiDinhMucTxtBox.Location = new System.Drawing.Point(192, 249);
            this.metKhoiVuotiDinhMucTxtBox.Name = "metKhoiVuotiDinhMucTxtBox";
            this.metKhoiVuotiDinhMucTxtBox.ReadOnly = true;
            this.metKhoiVuotiDinhMucTxtBox.Size = new System.Drawing.Size(190, 20);
            this.metKhoiVuotiDinhMucTxtBox.TabIndex = 15;
            // 
            // tongTienTxtBox
            // 
            this.tongTienTxtBox.Location = new System.Drawing.Point(192, 365);
            this.tongTienTxtBox.Name = "tongTienTxtBox";
            this.tongTienTxtBox.ReadOnly = true;
            this.tongTienTxtBox.Size = new System.Drawing.Size(192, 20);
            this.tongTienTxtBox.TabIndex = 21;
            // 
            // phiBaoVeMoiTruongTxtBox
            // 
            this.phiBaoVeMoiTruongTxtBox.Location = new System.Drawing.Point(192, 326);
            this.phiBaoVeMoiTruongTxtBox.Name = "phiBaoVeMoiTruongTxtBox";
            this.phiBaoVeMoiTruongTxtBox.ReadOnly = true;
            this.phiBaoVeMoiTruongTxtBox.Size = new System.Drawing.Size(192, 20);
            this.phiBaoVeMoiTruongTxtBox.TabIndex = 19;
            // 
            // tongTienSuDungTxtBox
            // 
            this.tongTienSuDungTxtBox.Location = new System.Drawing.Point(192, 282);
            this.tongTienSuDungTxtBox.Name = "tongTienSuDungTxtBox";
            this.tongTienSuDungTxtBox.ReadOnly = true;
            this.tongTienSuDungTxtBox.Size = new System.Drawing.Size(192, 20);
            this.tongTienSuDungTxtBox.TabIndex = 17;
            // 
            // khachHangComboBox
            // 
            this.khachHangComboBox.FormattingEnabled = true;
            this.khachHangComboBox.Location = new System.Drawing.Point(192, 95);
            this.khachHangComboBox.Name = "khachHangComboBox";
            this.khachHangComboBox.Size = new System.Drawing.Size(192, 21);
            this.khachHangComboBox.TabIndex = 3;
            // 
            // noteTxtBox
            // 
            this.noteTxtBox.BackColor = System.Drawing.Color.RoyalBlue;
            this.noteTxtBox.Font = new System.Drawing.Font("Segoe UI Light", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.noteTxtBox.ForeColor = System.Drawing.Color.White;
            this.noteTxtBox.Location = new System.Drawing.Point(449, 208);
            this.noteTxtBox.Multiline = true;
            this.noteTxtBox.Name = "noteTxtBox";
            this.noteTxtBox.ReadOnly = true;
            this.noteTxtBox.Size = new System.Drawing.Size(264, 70);
            this.noteTxtBox.TabIndex = 22;
            // 
            // err
            // 
            this.err.ContainerControl = this;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Segoe UI Light", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.ForeColor = System.Drawing.Color.Maroon;
            this.label14.Location = new System.Drawing.Point(444, 103);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(99, 30);
            this.label14.TabIndex = 24;
            this.label14.Text = "Cách tính:";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Segoe UI Light", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(444, 316);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(90, 30);
            this.label15.TabIndex = 25;
            this.label15.Text = "Hoá đơn";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(877, 562);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.noteTxtBox);
            this.Controls.Add(this.khachHangComboBox);
            this.Controls.Add(this.tongTienSuDungTxtBox);
            this.Controls.Add(this.phiBaoVeMoiTruongTxtBox);
            this.Controls.Add(this.tongTienTxtBox);
            this.Controls.Add(this.metKhoiVuotiDinhMucTxtBox);
            this.Controls.Add(this.soNhanKhauTxtBox);
            this.Controls.Add(this.metKhoiTrongDinhMucTxtBox);
            this.Controls.Add(this.tieuThuMetKhoiTxtBox);
            this.Controls.Add(this.chiSoMoiTxtBox);
            this.Controls.Add(this.chiSoCuTxtBox);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.printTxtBox);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.MaximizeBox = false;
            this.Name = "Form1";
            this.Text = "Tính tiền nước";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.err)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button calBtn;
        private System.Windows.Forms.Button printBtn;
        private System.Windows.Forms.Button exitBtn;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.TextBox printTxtBox;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox chiSoCuTxtBox;
        private System.Windows.Forms.TextBox chiSoMoiTxtBox;
        private System.Windows.Forms.TextBox tieuThuMetKhoiTxtBox;
        private System.Windows.Forms.TextBox metKhoiTrongDinhMucTxtBox;
        private System.Windows.Forms.TextBox soNhanKhauTxtBox;
        private System.Windows.Forms.TextBox metKhoiVuotiDinhMucTxtBox;
        private System.Windows.Forms.TextBox tongTienTxtBox;
        private System.Windows.Forms.TextBox phiBaoVeMoiTruongTxtBox;
        private System.Windows.Forms.TextBox tongTienSuDungTxtBox;
        private System.Windows.Forms.ComboBox khachHangComboBox;
        private System.Windows.Forms.TextBox noteTxtBox;
        private System.Windows.Forms.ErrorProvider err;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
    }
}

